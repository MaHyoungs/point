package com.news;

import com.dto.CommonMap;
import com.login.security.CustomUserDetails;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/news")
public class newsController{

	private final Logger logger = LoggerFactory.getLogger(com.news.newsController.class);

	@Autowired
	private SqlSession sqlSession;


	/**
	 * 새소식
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/view.do")
	public String newsView(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("newsController.view !!");

		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		model.addAttribute("userDetails", userDetails);

		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);
		model.addAttribute("viewType", "new");

		// 새 고객
		model.addAttribute("newC", Integer.parseInt(sqlSession.selectOne("clientMapper.newClient", map).toString()));
		// 신규 포인트
		model.addAttribute("newP", Integer.parseInt(sqlSession.selectOne("pointMapper.newPoint", map).toString()));
		// 사용 포인트
		model.addAttribute("useP", Integer.parseInt(sqlSession.selectOne("pointMapper.usePoint", map).toString()));
		// 누적 포인트
		model.addAttribute("totP", Integer.parseInt(sqlSession.selectOne("pointMapper.totalPoint", map).toString()));
		// 고객 리스트
		model.addAttribute("listClient", sqlSession.selectList("clientMapper.listClient2", map));
		// 적립 통계
		model.addAttribute("statsSavePoint", sqlSession.selectList("pointMapper.statsSavePoint", map));
		// 사용 통계
		model.addAttribute("statsUsePoint", sqlSession.selectList("pointMapper.statsUsePoint", map));

		return "news/view.tiles";
	}
}