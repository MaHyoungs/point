package com.point;

import com.login.security.CustomUserDetails;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.dto.CommonMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/point")
public class PointController{

	private final Logger logger = LoggerFactory.getLogger(PointController.class);

	@Autowired
	private SqlSession sqlSession;


	/**
	 * 포인트 리스트
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list.do")
	public String pointList(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("PointController.list !!");
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		model.addAttribute("userDetails", userDetails);

		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);
		model.addAttribute("viewType", "point");

		model.addAttribute("listPoint", sqlSession.selectList("pointMapper.listPoint", map));

		return "point/list.tiles";
	}

	/**
	 * 포인트 등록
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insert.do")
	public String pointInsert(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("PointController.insert !!");

		CommonMap map = new CommonMap(request);

		int pidx = sqlSession.insert("pointMapper.insertPoint", map);

		// 포인트 적립
		if(map.get("type").equals("01")) {
			int result = Integer.parseInt(map.get("point").toString()) + Integer.parseInt(map.get("originalpoint2").toString());
			map.put("resultpoint", result);
		}
		int uidx = sqlSession.update("clientMapper.updateClientPoint", map);


		return "redirect:/point/list.do";
	}

	/**
	 * 포인트 수정
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update.do")
	public String pointUpdate(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("PointController.update !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("msg", map.get("msg"));

		return "point/view";
	}

	/**
	 * 포인트 삭제
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete.do")
	public String pointDelete(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("PointController.delete !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("msg", map.get("msg"));

		return "point/view";
	}
}