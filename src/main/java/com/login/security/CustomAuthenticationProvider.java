package com.login.security;

import com.login.loginController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

public class CustomAuthenticationProvider implements AuthenticationProvider{

	private static final Logger logger = LoggerFactory.getLogger(loginController.class);

	@Value("#{prop['PW_ENCODER_KEY']}")
	private String PW_ENCODER_KEY;

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException{

		String user_id = (String)authentication.getPrincipal();
		String user_pw = (String)authentication.getCredentials();
		PasswordEncoder encoder = new ShaPasswordEncoder();
		String pwd = encoder.encodePassword(PW_ENCODER_KEY, user_pw);
		//logger.info("사용자가 입력한 로그인정보입니다. {}", user_id + "/" + user_pw);
		List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
		roles.add(new SimpleGrantedAuthority("ROLE_USER"));

		UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(user_id, pwd, roles);
		result.setDetails(new CustomUserDetails(user_id, pwd, "", "", ""));
		return result;

		/*if(chk > 0){
			logger.info("login Success");
			List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
			roles.add(new SimpleGrantedAuthority("ROLE_USER"));

			UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(user_id, pwd, roles);
			result.setDetails(new CustomUserDetails(user_id, pwd, "10"));
			return result;
		}else{
			//logger.info("사용자 크리덴셜 정보가 틀립니다. 에러가 발생합니다.");
			logger.info("login Fail");
			throw new BadCredentialsException("Bad credentials");
		}*/
	}
}