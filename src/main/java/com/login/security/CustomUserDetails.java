package com.login.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomUserDetails implements UserDetails{

	private static final long serialVersionUID = -4450269958885980297L;
	private String username;
	private String password;
	private String userauth;
	private String userNm;
	private String place;

	public CustomUserDetails(String userName, String password, String userauth, String userNm, String place)
	{
		this.username = userName;
		this.password = password;
		this.userauth = userauth;
		this.userNm = userNm;
		this.place = place;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	public String getUserauth() {
		return userauth;
	}

	public void setUserauth(String userauth) {
		this.userauth = userauth;
	}

	public String getUserNm(){
		return userNm;
	}

	public void setUserNm(String userNm){
		this.userNm = userNm;
	}

	public String getPlace(){
		return place;
	}

	public void setPlace(String place){
		this.place = place;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
