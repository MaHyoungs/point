package com.login;

import com.dto.CommonMap;
import com.login.security.CustomUserDetails;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@Controller
public class loginController{

	private final Logger logger = LoggerFactory.getLogger(loginController.class);

	@Autowired
	private SqlSession sqlSession;


	/**
	 * 로그인 뷰
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "login.do")
	public String login() throws Exception {
		logger.info("loginController.login !!");
		return "cmm/login/login";
	}

	/**
	 * 로그아웃
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "logout.do", method = RequestMethod.GET)
	public String logout(HttpSession session) throws Exception {
		logger.info("loginController.logout !!");
		session.invalidate();
		return "redirect:/login.do";
	}


	/**
	 * 로그인
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "login_action.do")
	public String login_success(HttpSession session) {
		logger.info("loginController.login_action !!");
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		CommonMap user = new CommonMap();
		user.put("user_id" , userDetails.getUsername());
		user.put("user_pw" , userDetails.getPassword());
		CommonMap info = (CommonMap)sqlSession.selectOne("loginMapper.userChk", user);
		if(info != null){
			userDetails.setUserauth(info.get("user_auth").toString());
			userDetails.setUserNm(info.get("user_name").toString());
			session.setAttribute("userLoginInfo", userDetails);
			return "redirect:/news/view.do";
		} else {
			session.invalidate();
			return "redirect:/login.do";
		}

	}

	@RequestMapping(value = "login_duplicate.do")
	public void login_duplicate() {
		logger.info("Welcome login_duplicate!");
	}
}
