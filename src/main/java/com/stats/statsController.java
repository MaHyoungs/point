package com.stats;

import com.dto.CommonMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/stats")
public class statsController{

	private final Logger logger = LoggerFactory.getLogger(statsController.class);

	/**
	 * 통계 뷰
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/view.do")
	public String viewStats(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("statsController.view !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);
		model.addAttribute("viewType", "stats");

		return "stats/view.tiles";
	}
}