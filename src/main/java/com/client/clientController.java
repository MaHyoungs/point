package com.client;

import com.dto.CommonMap;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/client")
public class clientController{

	private final Logger logger = LoggerFactory.getLogger(clientController.class);

	@Autowired
	private SqlSession sqlSession;


	/**
	 * 고객 리스트
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list.do")
	public String clientList(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("clientController.list !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);
		model.addAttribute("viewType", "client");

		model.addAttribute("listClient", sqlSession.selectList("clientMapper.listClient", map));

		return "client/list.tiles";
	}

	/**
	 * 고객 등록
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/insert.do")
	public String clientInsert(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("clientController.insert !!");

		CommonMap map = new CommonMap(request);

		// 가입 여부 체크
		int didx = Integer.parseInt(sqlSession.selectOne("clientMapper.dupleChkClient", map).toString());

		if(didx > 0) {
			return "redirect:/client/list.do?msg=D";
		} else{
			// 고객 등록
			int iidx = sqlSession.insert("clientMapper.insertClient", map);
			CommonMap info = (CommonMap)sqlSession.selectOne("clientMapper.selectDetailClient", map);
			// 포인트 등록
			if(!map.get("point").equals("0")){
				map.put("seq_num", info.get("seq_num"));
				map.put("type", "01");
				int pidx = sqlSession.insert("pointMapper.insertPoint", map);
			}
			return "redirect:/client/list.do?msg=Y";
		}
	}

	/**
	 * 고객 수정
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update.do")
	public String clientUpdate(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("clientController.update !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("msg", map.get("msg"));

		return "client/list.tiles";
	}

	/**
	 * 고객 삭제
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete.do")
	public String clientDelete(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("clientController.delete !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("msg", map.get("msg"));

		return "client/list.tiles";
	}
}