<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<div class="row">
		<ol class="breadcrumb">
			<li><a href="/news/view.do"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active">새소식</li>
		</ol>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">새소식</h1>
		</div>
	</div>

	<div class="row">
      <div class="col-xs-12 col-md-6 col-lg-3">
			<div class="panel panel-teal panel-widget">
				<div class="row no-padding">
					<div id="newClients" class="col-sm-3 col-lg-5 widget-left mouse-type layerpopupOpen" data-popup-class="newClientAdd">
						<svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="large"><fmt:formatNumber value="${newC}" pattern="#,###,###,###"/></div>
						<div class="text-muted">새 적립 고객</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6 col-lg-3">
			<div class="panel panel-blue panel-widget ">
				<div class="row no-padding">
					<div class="col-sm-3 col-lg-5 widget-left mouse-type layerpopupOpen" data-popup-class="searchClient">
						<svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="large"><fmt:formatNumber value="${newP}" pattern="#,###,###,###"/> P</div>
						<div class="text-muted">오늘 추가된 포인트</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6 col-lg-3">
			<div class="panel panel-orange panel-widget">
				<div class="row no-padding">
					<div class="col-sm-3 col-lg-5 widget-left mouse-type layerpopupOpen" data-popup-class="searchClient2">
						<svg class="glyph stroked trash"><use xlink:href="#stroked-trash"></use></svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="large"><fmt:formatNumber value="${useP}" pattern="#,###,###,###"/> P</div>
						<div class="text-muted">오늘 사용된 포인트</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6 col-lg-3">
			<div class="panel panel-red panel-widget">
				<div class="row no-padding">
					<div id="totalPoint" class="col-sm-3 col-lg-5 widget-left mouse-type">
						<svg class="glyph stroked paperclip"><use xlink:href="#stroked-paperclip"></use></svg>
					</div>
					<div class="col-sm-9 col-lg-7 widget-right">
						<div class="large"><fmt:formatNumber value="${totP}" pattern="#,###,###,###"/> P</div>
						<div class="text-muted">총 누계 포인트</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">이번 주 사용현황</div>
				<div class="panel-body">
					<div class="canvas-wrapper">
						<c:forEach var="sp1" items="${statsSavePoint}" varStatus="stsss">
							<c:if test="${stsss.last ne true}">
								<c:set var="chartData1" value="${chartData1}${sp1.cnt}," />
							</c:if>
							<c:if test="${stsss.last}">
								<c:set var="chartData1" value="${chartData1}${sp1.cnt}" />
							</c:if>
						</c:forEach>
						<c:forEach var="sp2" items="${statsUsePoint}" varStatus="stss">
							<c:if test="${stss.last ne true}">
								<c:set var="chartData2" value="${chartData2}${sp2.cnt}," />
							</c:if>
							<c:if test="${stss.last}">
								<c:set var="chartData2" value="${chartData2}${sp2.cnt}" />
							</c:if>
						</c:forEach>
						<script>
							var lineChartData = {
								labels : ["월요일","화요일","수요일","목요일","금요일","토요일","일요일"],
								datasets : [
									{
										label: "적립",
										fillColor : "rgba(48,165,255,0.2)",
										strokeColor : "rgba(48,165,255,1)",
										pointColor : "rgba(48,165,255,1)",
										pointStrokeColor : "#fff",
										pointHighlightFill : "#fff",
										pointHighlightStroke : "rgba(48,165,255,1)",
										data : [${chartData1}]
									},
									{
										label: "사용",
										fillColor : "rgba(255, 181, 62, 0.2)",
										strokeColor : "rgba(255, 181, 62, 1)",
										pointColor : "rgba(255, 181, 62, 1)",
										pointStrokeColor : "#fff",
										pointHighlightFill : "#fff",
										pointHighlightStroke : "rgba(255, 181, 62, 1)",
										data : [${chartData2}]
									}
								]

							}
						</script>
						<canvas class="main-chart" id="line-chart" height="200" width="600"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%-- Layer --%>
	<div class="layerBg newClientAdd"></div>
	<div class="layerPopup newClientAdd style01">
		<div class="layerHeader">
			<span class="title">새 고객 추가</span>
			<a href="#" class="layerClose"><img src="/template/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
		</div>
		<div class="layerContainer">
			<div class="conContainer">
				<div class="mainTableWrap">
					<form id="clientForm" name="clientForm" action="/client/insert.do" method="post">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						<table class="style03" summary="">
							<caption></caption>
							<colgroup>
								<col width="36%" />
								<col width="64%" />
							</colgroup>
							<tbody>
								<tr>
									<th class="icon_tel">연락처</th>
									<td>
										<input type="text" id="clientPhone" name="phone" placeholder=" -없이 입력" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"/>
									</td>
								</tr>
								<tr>
									<th class="icon_name">이름</th>
									<td colspan="4">
										<input type="text" id="clientName" name="name" />
									</td>
								</tr>
								<tr>
									<th class="icon_comment">사용 금액</th>
									<td>
										<input type="text" id="clientBill" name="bill" maxlength="8" onkeyup="pointCal(this)"/>
									</td>
								</tr>
								<tr>
									<th class="icon_comment">적립 포인트</th>
									<td>
										<input type="text" id="clientPoint" name="point" readonly="readonly"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<div class="conFooter">
				<span class="saBtn">
					<a href="javascript:void(0)" onclick="newClient()" class="btnLayerGreen">등록</a>
					<a href="javascript:void(0)" onclick="reset('clientForm')" class="btnLayerGray layerClose">취소</a>
				</span>
			</div>
		</div>
	</div>
	<%-- Layer --%>

	<%-- Layer --%>
	<div class="layerBg searchClient"></div>
	<div class="layerPopup searchClient style02">
		<div class="layerHeader">
			<span class="title">사용자 검색</span>
			<a href="#" class="layerClose"><img src="/template/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
		</div>
		<div class="layerContainer">
			<h1 class="info">* 사용자를 선택하세요.</h1>
			<div class="conContainer">
				<div class="tableWrap">
					<table data-toggle="table" data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="true" data-pagination="true">
					    <thead>
						    <tr>
							    <th data-field="no" data-sortable="true">번호</th>
						        <th data-field="phone" data-sortable="true">연락처</th>
						        <th data-field="name"  data-sortable="true">이름</th>
						        <th data-field="point" data-sortable="true">보유포인트</th>
							    <th data-field="date" data-sortable="true">가입일시</th>
						    </tr>
					    </thead>
						<tbody>
							<c:forEach var="rs" items="${listClient}" varStatus="sts">
								<tr data-index="${sts.count}">
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('save', '${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${sts.count}</a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('save', '${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${rs.phone}</a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('save', '${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${rs.name}</a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('save', '${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${rs.point}</a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('save', '${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${rs.reg_dt}</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="conFooter">
				<span class="saBtn">
					<a href="#" class="btnSurveyLayerStart layerClose">닫기</a>
				</span>
			</div>
		</div>
	</div>
	<%-- Layer --%>

	<%-- Layer --%>
	<div class="layerBg pointSavePop"></div>
	<div class="layerPopup pointSavePop style01">
		<div class="layerHeader">
			<span class="title">포인트 적립</span>
			<a href="#" class="layerClose"><img src="/template/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
		</div>
		<div class="layerContainer">
			<div class="conContainer">
				<div class="mainTableWrap">
					<form id="pointSaveForm" name="pointSaveForm" action="/point/insert.do" method="post">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						<input type="hidden" id="savePointSeq" name="seq_num" />
						<input type="hidden" name="type" value="01" />
						<input type="hidden" id="originalPoint2" name="originalPoint2" />
						<table class="style03" summary="">
							<caption></caption>
							<colgroup>
								<col width="36%" />
								<col width="64%" />
							</colgroup>
							<tbody>
								<tr>
									<th class="icon_tel">연락처</th>
									<td>
										<input type="text" id="savePointPhone" name="phone" readonly="readonly"/>
									</td>
								</tr>
								<tr>
									<th class="icon_name">이름</th>
									<td colspan="4">
										<input type="text" id="savePointName" name="name" readonly="readonly"/>
									</td>
								</tr>
								<tr>
									<th class="icon_repasscheck">구분</th>
									<td colspan="4">
										<input type="text" name="pointTypeText" readonly="readonly" value="적립"/>
									</td>
								</tr>
								<tr>
									<th class="icon_comment">사용 금액</th>
									<td>
										<input type="text" id="savePointBill" name="bill" maxlength="8" onkeyup="pointCal2(this)"/>
									</td>
								</tr>
								<tr>
									<th class="icon_comment">적립 포인트</th>
									<td>
										<input type="text" id="savePointPoint" name="point" readonly="readonly"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<div class="conFooter">
				<span class="saBtn">
					<a href="javascript:void(0)" onclick="doPoint('save')" class="btnLayerGreen">등록</a>
					<a href="javascript:void(0)" onclick="reset('pointSaveForm')" class="btnLayerGray layerClose">취소</a>
				</span>
			</div>
		</div>
	</div>
	<%-- Layer --%>

	<%-- Layer --%>
	<div class="layerBg searchClient2"></div>
	<div class="layerPopup searchClient2 style02">
		<div class="layerHeader">
			<span class="title">사용자 검색</span>
			<a href="#" class="layerClose"><img src="/template/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
		</div>
		<div class="layerContainer">
			<h1 class="info">* 사용자를 선택하세요.</h1>
			<div class="conContainer">
				<div class="tableWrap">
					<table data-toggle="table" data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="true" data-pagination="true">
					    <thead>
						    <tr>
							    <th data-field="no" data-sortable="true">번호</th>
						        <th data-field="phone" data-sortable="true">연락처</th>
						        <th data-field="name"  data-sortable="true">이름</th>
						        <th data-field="point" data-sortable="true">보유포인트</th>
							    <th data-field="date" data-sortable="true">가입일시</th>
						    </tr>
					    </thead>
						<tbody>
							<c:forEach var="rs" items="${listClient}" varStatus="sts">
								<tr data-index="${sts.count}">
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('use', '${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${sts.count}</a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('use', '${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${rs.phone}</a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('use', '${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${rs.name}</a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('use', '${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${rs.point}</a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('use', '${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${rs.reg_dt}</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="conFooter">
				<span class="saBtn">
					<a href="#" class="btnSurveyLayerStart layerClose">닫기</a>
				</span>
			</div>
		</div>
	</div>
	<%-- Layer --%>

	<%-- Layer --%>
	<div class="layerBg pointUsePop"></div>
	<div class="layerPopup pointUsePop style01">
		<div class="layerHeader">
			<span class="title">포인트 사용</span>
			<a href="#" class="layerClose"><img src="/template/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
		</div>
		<div class="layerContainer">
			<div class="conContainer">
				<div class="mainTableWrap">
					<form id="pointUseForm" name="pointUseForm" action="/point/insert.do" method="post">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						<input type="hidden" id="usePointSeq" name="seq_num" />
						<input type="hidden" name="type" value="02" />
						<input type="hidden" id="originalPoint" name="originalPoint"/>
						<table class="style03" summary="">
							<caption></caption>
							<colgroup>
								<col width="36%" />
								<col width="64%" />
							</colgroup>
							<tbody>
								<tr>
									<th class="icon_tel">연락처</th>
									<td>
										<input type="text" id="usePointPhone" name="phone" readonly="readonly"/>
									</td>
								</tr>
								<tr>
									<th class="icon_name">이름</th>
									<td colspan="4">
										<input type="text" id="usePointName" name="name" readonly="readonly"/>
									</td>
								</tr>
								<tr>
									<th class="icon_repasscheck">구분</th>
									<td colspan="4">
										<input type="text" name="pointTypeText" readonly="readonly" value="사용"/>
									</td>
								</tr>
								<tr>
									<th class="icon_comment">사용 포인트</th>
									<td>
										<input type="text" id="usePoint" name="point" maxlength="8" onkeyup="pointCal3(this)"/>
									</td>
								</tr>
								<tr>
									<th class="icon_comment">남은 포인트</th>
									<td>
										<input type="text" id="totalResultPoint" name="resultPoint" readonly="readonly"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<div class="conFooter">
				<span class="saBtn">
					<a href="javascript:void(0)" onclick="doPoint('use')" class="btnLayerGreen">등록</a>
					<a href="javascript:void(0)" onclick="reset('pointUseForm')" class="btnLayerGray layerClose">취소</a>
				</span>
			</div>
		</div>
	</div>
	<%-- Layer --%>

<script>
	$(document).ready(function() {
		$('#newClients').click(function() {
			$('#clientPhone').focus();
		});

		$('#totalPoint').click(function() {
			location.href="/pln/searchSchoolPlannerIntro.do";
		});
	});

	$(document).keypress(function (e) {
		if (e.which == 13) {
			if($('#clientName').val() != '') {
				newClient();
			} else if($('#clientName').val() != '') {
				alert("포인트 적립");
			}
		}
	});

	function fnPopupOpen(popupId, zIndex){
		var  winWid = $(window).width()
				,winHei = $(window).height()
				,popupWid = $(".layerPopup."+popupId).width()
				,popupHei = $(".layerPopup."+popupId).height()
				,popupTop = (winHei-popupHei)/2
				,popupLeft = (winWid-popupWid)/2;

		$(".layerBg."+popupId).css({"display":"block"}).attr("data-popup-this", popupId);
		$(".layerPopup."+popupId).css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", popupId);
	}

	function newClient() {

		if($('#clientName').val() == ''){
			alert("이름을 입력하여 주십시요.");
			$('#clientName').focus();
			return false;
		}
		if($('#clientPhone').val() == ''){
			alert("연락처를 입력하여 주십시요.");
			$('#clientPhone').focus();
			return false;
		}

		if($('#point').val() == '') {
			$('#point').val('0');
		}

		document.clientForm.submit();
	}

	function reset(frm) {
		document.getElementById(frm).reset();
	}

	function pointCal(e) {
		e.value=e.value.replace(/[^0-9]/g,'');
		var result = e.value / 100 * 5;
		$('#clientPoint').val(parseInt(result));
	}

	function pointCal2(e) {
		e.value=e.value.replace(/[^0-9]/g,'');
		var result = e.value / 100 * 5;
		$('#savePointPoint').val(parseInt(result));
	}

	function pointCal3(e) {
		e.value=e.value.replace(/[^0-9]/g,'');
		var original = $('#originalPoint').val();
		var result = (original - e.value);
		$('#totalResultPoint').val(parseInt(result));
	}

	function doPointPop(type,seq,phone,name,point) {

		// 포인트 적립
		if(type == 'save') {
			popupMessageDown('searchClient.style02', 'searchClient');
			$('#savePointSeq').val(seq);
			$('#savePointPhone').val(phone);
			$('#savePointName').val(name);
			$('#originalPoint2').val(point);
			fnPopupOpen('pointSavePop');
			$('#savePointBill').focus();

		// 포인트 사용
		} else {
			popupMessageDown('searchClient2.style02', 'searchClient2');
			$('#usePointSeq').val(seq);
			$('#usePointPhone').val(phone);
			$('#usePointName').val(name);
			$('#originalPoint').val(point);
			$('#totalResultPoint').val(point);
			fnPopupOpen('pointUsePop');
			$('#usePoint').focus();
		}
	}

	function doPoint(typ) {

		if(typ == 'save') {
			document.pointSaveForm.submit();
		} else {
			var totalPoint = $('#totalResultPoint').val();

			if(parseInt(totalPoint) < 0) {
				alert("잔여 포인트가 부족합니다.");
				return false;
			}
			document.pointUseForm.submit();
		}
	}
</script>