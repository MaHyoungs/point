<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

	<div class="row">
		<ol class="breadcrumb">
			<li><a href="/news/view.do"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active">고객</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">고객</h1>
		</div>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">고객 현황</div>
				<div class="panel-body">
					<div class="col-md-6">
						<button type="button" class="btn btn-default layerpopupOpen" data-popup-class="newClientAdd">등록</button>
					</div>
					<table data-toggle="table" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-pagination="true" data-sort-name="date" data-sort-order="desc">
					    <thead>
						    <tr>
							    <th data-field="no" data-sortable="true">번호</th>
						        <th data-field="phone" data-sortable="true">연락처</th>
						        <th data-field="name"  data-sortable="true">이름</th>
						        <th data-field="point" data-sortable="true">보유포인트</th>
							    <th data-field="date" data-sortable="true">가입일시</th>
						    </tr>
					    </thead>
						<tbody>
							<c:forEach var="rs" items="${listClient}" varStatus="sts">
								<tr data-index="${sts.count}">
									<td>${sts.count}</td>
									<td>${rs.phone}</td>
									<td>${rs.name}</td>
									<td>ⓟ ${rs.point}</td>
									<td>${rs.reg_dt}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<%-- Layer --%>
	<div class="layerBg newClientAdd"></div>
		<div class="layerPopup newClientAdd style01">
			<div class="layerHeader">
				<span class="title">새 고객 추가</span>
				<a href="#" class="layerClose"><img src="/template/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
			</div>
			<div class="layerContainer">
				<div class="conContainer">
					<div class="mainTableWrap">
						<form id="clientForm" name="clientForm" action="/client/insert.do" method="post">
							<table class="style03" summary="">
								<caption></caption>
								<colgroup>
									<col width="36%" />
									<col width="64%" />
								</colgroup>
								<tbody>
									<tr>
										<th class="icon_tel">연락처</th>
										<td>
											<input type="text" id="clientPhone" name="phone" placeholder=" -없이 입력" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"/>
										</td>
									</tr>
									<tr>
										<th class="icon_name">이름</th>
										<td colspan="4">
											<input type="text" id="clientName" name="name" />
										</td>
									</tr>
									<tr>
										<th class="icon_comment">사용 금액</th>
										<td>
											<input type="text" id="clientBill" name="bill" maxlength="8" onkeyup="pointCal(this)"/>
										</td>
									</tr>
									<tr>
										<th class="icon_comment">적립 포인트</th>
										<td>
											<input type="text" id="clientPoint" name="point" readonly="readonly"/>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
				</div>
				<div class="conFooter">
					<span class="saBtn">
						<a href="javascript:void(0)" onclick="newClient()" class="btnLayerGreen">등록</a>
						<a href="javascript:void(0)" onclick="reset('clientForm')" class="btnLayerGray layerClose">취소</a>
					</span>
				</div>
			</div>
		</div>
	<%-- Layer --%>


<script>
	$(document).ready(function() {
		<c:if test="${not empty map.msg}">
			<c:choose>
				<c:when test="${map.msg eq 'D'}">
					alert("이름과 연락처가 같은 고객이 있습니다.");
				</c:when>
				<c:when test="${map.msg eq 'Y'}">
					alert("새 고객이 등록되었습니다.");
				</c:when>
			</c:choose>
		</c:if>
	});

	function newClient() {

		if($('#clientName').val() == ''){
			alert("이름을 입력하여 주십시요.");
			$('#clientName').focus();
			return false;
		}
		if($('#clientPhone').val() == ''){
			alert("연락처를 입력하여 주십시요.");
			$('#clientPhone').focus();
			return false;
		}

		if($('#point').val() == '') {
			$('#point').val('0');
		}

		document.clientForm.submit();
	}

	function reset(frm) {
		document.getElementById(frm).reset();
	}

	function pointCal(e) {
		e.value=e.value.replace(/[^0-9]/g,'');
		var result = e.value / 100 * 5;
		$('#clientPoint').val(parseInt(result));
	}
</script>