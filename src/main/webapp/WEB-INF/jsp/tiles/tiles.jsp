<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>포인트 적립</title>
		<link rel="shortcut icon" href="/favicon.ico" />
		<meta name="description" content="포인트 적립 시스템">
		<meta property="og:type" content="website">
		<meta property="og:title" content="포인트 적립">

		<!-- JavaScript import -->
		<link href="/template/css/bootstrap.min.css" rel="stylesheet">
		<link href="/template/css/datapicker3.css" rel="stylesheet">
		<link href="/template/css/bootstrap-table.css" rel="stylesheet">
		<link href="/template/css/styles.css" rel="stylesheet">
		<link href="/template/css/common.css" rel="stylesheet">
		<%--<link href="./template/css/bootstrap.min.css" rel="stylesheet">
		<link href="./template/css/datepicker3.css" rel="stylesheet">
		<link href="./template/css/bootstrap-table.css" rel="stylesheet">
		<link href="./template/css/styles.css" rel="stylesheet">
		<link href="./template/css/common.css" rel="stylesheet">--%>
		<!-- JavaScript import -->

		<!-- StyleSheet import -->
		<!--Icons-->
		<script src="/template/js/lumino.glyphs.js"></script>
		<script src="/template/js/jquery-1.12.3.min.js"></script>
		<script src="/template/js/bootstrap.min.js"></script>
		<script src="/template/js/chart.min.js"></script>
		<script src="/template/js/chart-data.js"></script>
		<script src="/template/js/easypiechrt.js"></script>
		<script src="/template/js/easypiechart-data.js"></script>
		<script src="/template/js/bootstrap-datepicker.js"></script>
		<script src="/template/js/bootstrap-table.js"></script>
		<script src="/template/js/layerpopup.js"></script>
		<%--<script src="./template/js/lumino.glyphs.js"></script>
		<script src="./template/js/jquery-1.12.3.min.js"></script>
		<script src="./template/js/bootstrap.min.js"></script>
		<script src="./template/js/chart.min.js"></script>
		<script src="./template/js/chart-data.js"></script>
		<script src="./template/js/easypiechart.js"></script>
		<script src="./template/js/easypiechart-data.js"></script>
		<script src="./template/js/bootstrap-datepicker.js"></script>
		<script src="./template/js/bootstrap-table.js"></script>
		<script src="./template/js/layerpopup.js"></script>--%>
		<script>
			$('#calendar').datepicker({
			});

			!function ($) {
				$(document).on("click","ul.nav li.parent > a > span.icon", function(){
					$(this).find('em:first').toggleClass("glyphicon-minus");
				});
				$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
			}(window.jQuery);

			$(window).on('resize', function () {
				if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
			})
			$(window).on('resize', function () {
				if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
			})
		</script>
		<!-- StyleSheet import -->
	</head>

	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Admin</a>
					<ul class="user-menu">
						<li class="dropdown pull-right">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> ${userDetails.userNm} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
								<%--<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>--%>
								<li><a href="/logout.do"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
			<%--<form role="search">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Search">
				</div>
			</form>--%>

			<br/><br/><br/><br/>

			<%-- LEFT 메뉴 --%>
			<ul class="nav menu">
				<li <c:if test="${viewType eq 'new'}">class="active"</c:if>><a href="/news/view.do"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> 새소식 </a></li>
				<li <c:if test="${viewType eq 'point'}">class="active"</c:if>><a href="/point/list.do"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg> 포인트 </a></li>
				<li <c:if test="${viewType eq 'client'}">class="active"</c:if>><a href="/client/list.do"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> 고객 </a></li>
				<li <c:if test="${viewType eq 'stats'}">class="active"</c:if>><a href="/stats/view.do"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> 통계 </a></li>
				<li role="presentation" class="divider"></li>
				<li <c:if test="${viewType eq 'info'}">class="active"</c:if>><a href="/user/info.do"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> 내정보 </a></li>
			</ul>
		</div>

		<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
			<tiles:insertAttribute name="content"/>
		</div>

		<c:import url="/WEB-INF/jsp/cmm/layout/layerMessage.jsp" charEncoding="utf-8" />
	</body>
</html>