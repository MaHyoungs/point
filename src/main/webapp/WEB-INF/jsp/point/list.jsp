<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

	<div class="row">
		<ol class="breadcrumb">
			<li><a href="/news/view.do"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active">포인트</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">포인트</h1>
		</div>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">포인트 사용현황</div>
				<div class="panel-body">
					<table data-toggle="table" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-pagination="true" data-sort-name="date" data-sort-order="desc">
					    <thead>
						    <tr>
							    <th data-field="no" data-sortable="true">번호</th>
						        <th data-field="phone" data-sortable="true">휴대폰번호</th>
							    <th data-field="name" data-sortable="true">이름</th>
						        <th data-field="type"  data-sortable="true">구분</th>
						        <th data-field="point" data-sortable="true">포인트</th>
							    <th data-field="date" data-sortable="true">적용일시</th>
						    </tr>
					    </thead>
						<tbody>
							<c:forEach var="rs" items="${listPoint}" varStatus="sts">
								<tr data-index="${sts.count}">
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${sts.count}</a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${rs.phone}</a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${rs.name}</a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray"><c:if test="${rs.type eq '01'}">적립</c:if><c:if test="${rs.type eq '02'}">사용</c:if></a></td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">ⓟ ${rs.point}</td>
									<td class="mouse-type"><a href="javascript:void(0)" onclick="doPointPop('${rs.seq_num}', '${rs.phone}', '${rs.name}', '${rs.point}')" class="font-gray">${rs.reg_dt}</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<%-- Layer --%>
	<div class="layerBg pointPop"></div>
	<div class="layerPopup pointPop style01">
		<div class="layerHeader">
			<span class="title">포인트</span>
			<a href="#" class="layerClose"><img src="/template/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
		</div>
		<div class="layerContainer">
			<div class="conContainer">
				<div class="mainTableWrap">
					<form id="pointForm" name="pointForm" method="post">
						<input type="hidden" id="pointSeq" name="seq_num" />
						<input type="hidden" id="originalPoint" name="originalPoint"/>
						<table class="style03" summary="">
							<caption></caption>
							<colgroup>
								<col width="36%" />
								<col width="64%" />
							</colgroup>
							<tbody>
								<tr>
									<th class="icon_repasscheck">구분</th>
									<td colspan="4">
										<label class="font15">
											<input type="radio" class="hei22" id="pointSave" value="option1" checked>적립
										</label>
										<label class="font15">
											<input type="radio" class="hei22" id="pointUse" value="option1" checked>사용
										</label>
									</td>
								</tr>
								<tr>
									<th class="icon_tel">연락처</th>
									<td>
										<input type="text" id="pointPhone" name="phone" readonly="readonly"/>
									</td>
								</tr>
								<tr>
									<th class="icon_name">이름</th>
									<td colspan="4">
										<input type="text" id="pointName" name="name" readonly="readonly"/>
									</td>
								</tr>
								<tr id="saveTr1">
									<th class="icon_comment">사용 금액</th>
									<td>
										<input type="text" id="savePointBill" name="bill" maxlength="8" onkeyup="pointCal2(this)"/>
									</td>
								</tr>
								<tr id="saveTr2">
									<th class="icon_comment">적립 포인트</th>
									<td>
										<input type="text" id="savePointPoint" name="point" readonly="readonly"/>
									</td>
								</tr>
								<tr id="useTr1" style="display:none;">
									<th class="icon_comment">사용 포인트</th>
									<td>
										<input type="text" id="usePoint" name="point" maxlength="8" onkeyup="pointCal3(this)"/>
									</td>
								</tr>
								<tr id="useTr2" style="display:none;">
									<th class="icon_comment">남은 포인트</th>
									<td>
										<input type="text" id="totalResultPoint" name="resultPoint" readonly="readonly"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<div class="conFooter">
				<span class="saBtn">
					<a href="javascript:void(0)" onclick="doPoint()" class="btnLayerGreen">등록</a>
					<a href="javascript:void(0)" onclick="reset('pointForm')" class="btnLayerGray layerClose">취소</a>
				</span>
			</div>
		</div>
	</div>
	<%-- Layer --%>

<script>

	function fnPopupOpen(popupId, zIndex){
		var  winWid = $(window).width()
				,winHei = $(window).height()
				,popupWid = $(".layerPopup."+popupId).width()
				,popupHei = $(".layerPopup."+popupId).height()
				,popupTop = (winHei-popupHei)/2
				,popupLeft = (winWid-popupWid)/2;

		$(".layerBg."+popupId).css({"display":"block"}).attr("data-popup-this", popupId);
		$(".layerPopup."+popupId).css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", popupId);
	}

	function doPointPop(seq,phone,name,point) {
		fnPopupOpen('pointPop');
		$('#pointSeq').val(seq);
		$('#originalPoint').val(point);
		$('#pointPhone').val(phone);
		$('#pointName').val(name);
	}

	function reset(frm) {
		document.getElementById(frm).reset();
	}
</script>