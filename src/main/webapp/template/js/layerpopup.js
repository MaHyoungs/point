jQuery(function($){
	/**
	 * layerpopupOpen클래스를 가진 태그 클릭시 해당 태그의 data-popup-class값을 갖는 팝업 열기
	 * 따로 클래스명을 주어서 띄울때는 fnPopupOpen()에 클래스명만 넣어서 띄우기 가능
	 */
	$(".layerpopupOpen").click(function(e){
		e.preventDefault();
		fnPopupOpen($(this).attr("data-popup-class"));
	});

	$(document).on("click", ".layerpopupOpen2", function(e){
		e.preventDefault();
		fnPopupOpen($(this).attr("data-popup-class"));
	});

	/**
	 * layerClose
	 */
	$(".layerPopup .layerClose").click(function(e){
		e.preventDefault();
		/**
		 * layerclose가 포함된 팝업구분자
		 * @type {*|jQuery}
		 * @private
		 */
		var _target = $(this).parents(".layerPopup").attr("data-popup-this");
		/**
		 * 레이어 팝업 안보이게
		 */
		$(this).parents(".layerPopup." + _target).css({"display":"none"});
		/**
		 * 레이어 배경 안보이게
		 */
		$(this).parents(".layerPopup." + _target).siblings(".layerBg." + _target).css({"display":"none"});
	});
	$(".layerBg").click(function(){
		var _target = $(this).attr("data-popup-this");
		/**
		 * 레이어 팝업 안보이게
		 */
		$(this).siblings(".layerPopup." + _target).css({"display":"none"});
		/**
		 * 레이어 배경 안보이게
		 */
		$(this).css({"display":"none"});
	});

	/**
	 * 팝업열기
	 * @param popupId
	 */
	function fnPopupOpen(popupId, zIndex){
		var  winWid = window.innerWidth
			,winHei = window.innerHeight
			,popupWid = $(".layerPopup."+popupId).width()
			,popupHei = $(".layerPopup."+popupId).height()
			,popupTop = (winHei-popupHei)/2
			,popupLeft = (winWid-popupWid)/2;

		/**
		 * 팝업 배경 블럭
		 */
		$(".layerBg."+popupId).css({"display":"block"}).attr("data-popup-this", popupId);
		/**
		 * 해당 클래스 가지는 팝업 표출
		 */
		$(".layerPopup."+popupId).css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", popupId);
	}

	$(".messageClose, .messageBg").click(function(e){
		e.preventDefault();
		$(this).parents(".messageLayerWrap").remove();
	});

});

function fnLayerResize(popupId){
	var  winWid = $(window).width()
		,winHei = $(window).height()
		,popupWid = $(".layerPopup."+popupId).width()
		,popupHei = $(".layerPopup."+popupId).height()
		,popupTop = (winHei-popupHei)/2
		,popupLeft = (winWid-popupWid)/2;
	$(".layerPopup."+popupId).css({"top":popupTop, "left":popupLeft});
}

function fnAlertMessage(_message){
	var  winWid = $(window).width()
		,winHei = $(window).height()
		,popupWid = 420
		,popupHei = 300
		,popupLeft = (winWid-popupWid)/2
		,popupTop = (winHei-popupHei)/2;
	$(".messagePopupClone").clone(true).appendTo("#wrap").removeClass("disNon").css({"display":"block"}).children(".messageLayer").css({"top":popupTop, "left":popupLeft}).children(".layerContainer").children(".message").html(_message);
}

// 확인 버튼 클릭 후 url 로 이동처리
function fnAlertUrlMessage(_message, url){
	var  winWid = $(window).width()
		,winHei = $(window).height()
		,popupWid = 420
		,popupHei = 300
		,popupLeft = (winWid-popupWid)/2
		,popupTop = (winHei-popupHei)/2;
	var messageLayer = $(".messagePopupClone").clone(true).appendTo("#wrap").removeClass("disNon").css({"display":"block"}).children(".messageLayer");
	messageLayer.css({"top":popupTop, "left":popupLeft});
	messageLayer.children(".layerContainer").children(".message").html(_message);
	messageLayer.children(".layerFooter").children(".saBtn").children("a").attr("href", "javascript:void(0)");
	var js_func = "location.href='"+url+"'";
	var clickEvent = new Function(js_func);
	messageLayer.children(".layerFooter").children(".saBtn").children("a").attr("onclick", "").click(clickEvent);
}

function popupMessageDown(popupId1, popupId2) {
	$(".layerPopup." + popupId1).css({"display":"none"});
	$(".layerBg." + popupId2).css({"display":"none"});
}

function fnPopupConfirmOpen(popupId){
	var  winWid = $(window).width()
		,winHei = $(window).height()
		,popupWid = $(".layerPopup."+popupId).width()
		,popupHei = $(".layerPopup."+popupId).height()
		,popupTop = (winHei-popupHei)/2
		,popupLeft = (winWid-popupWid)/2;

	$(".layerBg."+popupId).css({"display":"block"}).attr("data-popup-this", popupId);
	$(".layerPopup."+popupId).css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", popupId);
}